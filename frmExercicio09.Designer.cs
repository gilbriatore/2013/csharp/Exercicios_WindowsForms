﻿namespace Aula_03
{
    partial class frmExercicio09
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalcular = new System.Windows.Forms.Button();
            this.txtMt = new System.Windows.Forms.TextBox();
            this.lblMt = new System.Windows.Forms.Label();
            this.txtKm = new System.Windows.Forms.TextBox();
            this.lblKm = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(271, 35);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 9;
            this.btnCalcular.Text = "&Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // txtMt
            // 
            this.txtMt.Enabled = false;
            this.txtMt.Location = new System.Drawing.Point(156, 46);
            this.txtMt.Name = "txtMt";
            this.txtMt.Size = new System.Drawing.Size(100, 20);
            this.txtMt.TabIndex = 8;
            this.txtMt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblMt
            // 
            this.lblMt.AutoSize = true;
            this.lblMt.Location = new System.Drawing.Point(13, 49);
            this.lblMt.Name = "lblMt";
            this.lblMt.Size = new System.Drawing.Size(135, 13);
            this.lblMt.TabIndex = 7;
            this.lblMt.Text = "Valor convertido em metros";
            // 
            // txtKm
            // 
            this.txtKm.Location = new System.Drawing.Point(156, 20);
            this.txtKm.Name = "txtKm";
            this.txtKm.Size = new System.Drawing.Size(100, 20);
            this.txtKm.TabIndex = 6;
            this.txtKm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblKm
            // 
            this.lblKm.AutoSize = true;
            this.lblKm.Location = new System.Drawing.Point(13, 23);
            this.lblKm.Name = "lblKm";
            this.lblKm.Size = new System.Drawing.Size(98, 13);
            this.lblKm.TabIndex = 5;
            this.lblKm.Text = "Valor em kilometros";
            // 
            // frmExercicio09
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 83);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.txtMt);
            this.Controls.Add(this.lblMt);
            this.Controls.Add(this.txtKm);
            this.Controls.Add(this.lblKm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExercicio09";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercício 09";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.TextBox txtMt;
        private System.Windows.Forms.Label lblMt;
        private System.Windows.Forms.TextBox txtKm;
        private System.Windows.Forms.Label lblKm;
    }
}