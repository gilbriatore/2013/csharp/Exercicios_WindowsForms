﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_03
{
    public partial class frmExercicio09 : Form
    {
        public frmExercicio09()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            float km, mt;
            km = float.Parse(txtKm.Text);
            mt = km * 1000;
            txtMt.Text = mt.ToString();
        }
    }
}
