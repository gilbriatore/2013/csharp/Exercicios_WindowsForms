﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_03
{
    public partial class frmExercicio10 : Form
    {
        public frmExercicio10()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int nascimento, atual, idade;
            nascimento = int.Parse(txtNascimento.Text);
            atual = int.Parse(txtAtual.Text);
            idade = atual - nascimento;
            txtIdade.Text = idade.ToString();
        }
    }
}
