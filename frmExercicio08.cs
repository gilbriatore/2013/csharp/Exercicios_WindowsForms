﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_03
{
    public partial class frmExercicio08 : Form
    {
        public frmExercicio08()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int a, b, c, soma;
            a = int.Parse(txtA.Text);
            b = int.Parse(txtB.Text);
            c = int.Parse(txtC.Text);
            soma = (a + b + c) * (a + b + c);
            txtSoma.Text = soma.ToString();
        }
    }
}
