﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_03
{
    public partial class frmExercicio05 : Form
    {
        public frmExercicio05()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            float bruto, inss, liquido;
            bruto = float.Parse(txtBruto.Text);
            inss = bruto * 9 / 100;
            liquido = bruto - inss;
            txtInss.Text = inss.ToString();
            txtLiquido.Text = liquido.ToString();
        }
    }
}
