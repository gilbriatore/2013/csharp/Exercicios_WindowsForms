﻿namespace Aula_03
{
    partial class frmExercicio04
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtB2 = new System.Windows.Forms.TextBox();
            this.lblB2 = new System.Windows.Forms.Label();
            this.txtB1 = new System.Windows.Forms.TextBox();
            this.lblB1 = new System.Windows.Forms.Label();
            this.txtB3 = new System.Windows.Forms.TextBox();
            this.lblB3 = new System.Windows.Forms.Label();
            this.txtB4 = new System.Windows.Forms.TextBox();
            this.lblB4 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtB2
            // 
            this.txtB2.Location = new System.Drawing.Point(124, 44);
            this.txtB2.Name = "txtB2";
            this.txtB2.Size = new System.Drawing.Size(100, 20);
            this.txtB2.TabIndex = 14;
            this.txtB2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB2
            // 
            this.lblB2.AutoSize = true;
            this.lblB2.Location = new System.Drawing.Point(17, 47);
            this.lblB2.Name = "lblB2";
            this.lblB2.Size = new System.Drawing.Size(92, 13);
            this.lblB2.TabIndex = 13;
            this.lblB2.Text = "Segundo bimestre";
            // 
            // txtB1
            // 
            this.txtB1.Location = new System.Drawing.Point(124, 16);
            this.txtB1.Name = "txtB1";
            this.txtB1.Size = new System.Drawing.Size(100, 20);
            this.txtB1.TabIndex = 12;
            this.txtB1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB1
            // 
            this.lblB1.AutoSize = true;
            this.lblB1.Location = new System.Drawing.Point(17, 19);
            this.lblB1.Name = "lblB1";
            this.lblB1.Size = new System.Drawing.Size(86, 13);
            this.lblB1.TabIndex = 11;
            this.lblB1.Text = "Primeiro bimestre";
            // 
            // txtB3
            // 
            this.txtB3.Location = new System.Drawing.Point(124, 76);
            this.txtB3.Name = "txtB3";
            this.txtB3.Size = new System.Drawing.Size(100, 20);
            this.txtB3.TabIndex = 16;
            this.txtB3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB3
            // 
            this.lblB3.AutoSize = true;
            this.lblB3.Location = new System.Drawing.Point(17, 79);
            this.lblB3.Name = "lblB3";
            this.lblB3.Size = new System.Drawing.Size(88, 13);
            this.lblB3.TabIndex = 15;
            this.lblB3.Text = "Terceiro bimestre";
            // 
            // txtB4
            // 
            this.txtB4.Enabled = false;
            this.txtB4.Location = new System.Drawing.Point(124, 108);
            this.txtB4.Name = "txtB4";
            this.txtB4.Size = new System.Drawing.Size(100, 20);
            this.txtB4.TabIndex = 18;
            this.txtB4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB4
            // 
            this.lblB4.AutoSize = true;
            this.lblB4.Location = new System.Drawing.Point(17, 111);
            this.lblB4.Name = "lblB4";
            this.lblB4.Size = new System.Drawing.Size(81, 13);
            this.lblB4.TabIndex = 17;
            this.lblB4.Text = "Quarto bimestre";
            // 
            // btnCalcular
            // 
            this.btnCalcular.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCalcular.Location = new System.Drawing.Point(149, 136);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 19;
            this.btnCalcular.Text = "&Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // frmExercicio04
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(249, 180);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.txtB4);
            this.Controls.Add(this.lblB4);
            this.Controls.Add(this.txtB3);
            this.Controls.Add(this.lblB3);
            this.Controls.Add(this.txtB2);
            this.Controls.Add(this.lblB2);
            this.Controls.Add(this.txtB1);
            this.Controls.Add(this.lblB1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExercicio04";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercício 04";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtB2;
        private System.Windows.Forms.Label lblB2;
        private System.Windows.Forms.TextBox txtB1;
        private System.Windows.Forms.Label lblB1;
        private System.Windows.Forms.TextBox txtB3;
        private System.Windows.Forms.Label lblB3;
        private System.Windows.Forms.TextBox txtB4;
        private System.Windows.Forms.Label lblB4;
        private System.Windows.Forms.Button btnCalcular;
    }
}