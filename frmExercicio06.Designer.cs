﻿namespace Aula_03
{
    partial class frmExercicio06
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtN1 = new System.Windows.Forms.TextBox();
            this.lblN1 = new System.Windows.Forms.Label();
            this.txtN2 = new System.Windows.Forms.TextBox();
            this.lblN2 = new System.Windows.Forms.Label();
            this.txtFinalN1 = new System.Windows.Forms.TextBox();
            this.lblFinalN1 = new System.Windows.Forms.Label();
            this.txtFinalN2 = new System.Windows.Forms.TextBox();
            this.lblFinalN2 = new System.Windows.Forms.Label();
            this.btnTrocar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtN1
            // 
            this.txtN1.Location = new System.Drawing.Point(112, 16);
            this.txtN1.Name = "txtN1";
            this.txtN1.Size = new System.Drawing.Size(100, 20);
            this.txtN1.TabIndex = 16;
            this.txtN1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblN1
            // 
            this.lblN1.AutoSize = true;
            this.lblN1.Location = new System.Drawing.Point(17, 19);
            this.lblN1.Name = "lblN1";
            this.lblN1.Size = new System.Drawing.Size(72, 13);
            this.lblN1.TabIndex = 15;
            this.lblN1.Text = "Valor para N1";
            // 
            // txtN2
            // 
            this.txtN2.Location = new System.Drawing.Point(112, 44);
            this.txtN2.Name = "txtN2";
            this.txtN2.Size = new System.Drawing.Size(100, 20);
            this.txtN2.TabIndex = 18;
            this.txtN2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblN2
            // 
            this.lblN2.AutoSize = true;
            this.lblN2.Location = new System.Drawing.Point(17, 47);
            this.lblN2.Name = "lblN2";
            this.lblN2.Size = new System.Drawing.Size(72, 13);
            this.lblN2.TabIndex = 17;
            this.lblN2.Text = "Valor para N2";
            // 
            // txtFinalN1
            // 
            this.txtFinalN1.Enabled = false;
            this.txtFinalN1.Location = new System.Drawing.Point(112, 72);
            this.txtFinalN1.Name = "txtFinalN1";
            this.txtFinalN1.Size = new System.Drawing.Size(100, 20);
            this.txtFinalN1.TabIndex = 20;
            this.txtFinalN1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFinalN1
            // 
            this.lblFinalN1.AutoSize = true;
            this.lblFinalN1.Location = new System.Drawing.Point(17, 75);
            this.lblFinalN1.Name = "lblFinalN1";
            this.lblFinalN1.Size = new System.Drawing.Size(94, 13);
            this.lblFinalN1.TabIndex = 19;
            this.lblFinalN1.Text = "Valor final para N1";
            // 
            // txtFinalN2
            // 
            this.txtFinalN2.Enabled = false;
            this.txtFinalN2.Location = new System.Drawing.Point(112, 100);
            this.txtFinalN2.Name = "txtFinalN2";
            this.txtFinalN2.Size = new System.Drawing.Size(100, 20);
            this.txtFinalN2.TabIndex = 22;
            this.txtFinalN2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFinalN2
            // 
            this.lblFinalN2.AutoSize = true;
            this.lblFinalN2.Location = new System.Drawing.Point(17, 103);
            this.lblFinalN2.Name = "lblFinalN2";
            this.lblFinalN2.Size = new System.Drawing.Size(94, 13);
            this.lblFinalN2.TabIndex = 21;
            this.lblFinalN2.Text = "Valor final para N2";
            // 
            // btnTrocar
            // 
            this.btnTrocar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnTrocar.Location = new System.Drawing.Point(136, 128);
            this.btnTrocar.Name = "btnTrocar";
            this.btnTrocar.Size = new System.Drawing.Size(75, 23);
            this.btnTrocar.TabIndex = 24;
            this.btnTrocar.Text = "&Trocar";
            this.btnTrocar.UseVisualStyleBackColor = true;
            this.btnTrocar.Click += new System.EventHandler(this.btnTrocar_Click);
            // 
            // frmExercicio06
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(233, 166);
            this.Controls.Add(this.btnTrocar);
            this.Controls.Add(this.txtFinalN2);
            this.Controls.Add(this.lblFinalN2);
            this.Controls.Add(this.txtFinalN1);
            this.Controls.Add(this.lblFinalN1);
            this.Controls.Add(this.txtN2);
            this.Controls.Add(this.lblN2);
            this.Controls.Add(this.txtN1);
            this.Controls.Add(this.lblN1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExercicio06";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercicio 06";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtN1;
        private System.Windows.Forms.Label lblN1;
        private System.Windows.Forms.TextBox txtN2;
        private System.Windows.Forms.Label lblN2;
        private System.Windows.Forms.TextBox txtFinalN1;
        private System.Windows.Forms.Label lblFinalN1;
        private System.Windows.Forms.TextBox txtFinalN2;
        private System.Windows.Forms.Label lblFinalN2;
        private System.Windows.Forms.Button btnTrocar;
    }
}