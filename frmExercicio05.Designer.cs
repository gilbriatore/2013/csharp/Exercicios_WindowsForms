﻿namespace Aula_03
{
    partial class frmExercicio05
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.lblBruto = new System.Windows.Forms.Label();
            this.txtInss = new System.Windows.Forms.TextBox();
            this.lblInss = new System.Windows.Forms.Label();
            this.txtLiquido = new System.Windows.Forms.TextBox();
            this.lblLiquido = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtBruto
            // 
            this.txtBruto.Location = new System.Drawing.Point(128, 20);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.Size = new System.Drawing.Size(100, 20);
            this.txtBruto.TabIndex = 14;
            this.txtBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBruto
            // 
            this.lblBruto.AutoSize = true;
            this.lblBruto.Location = new System.Drawing.Point(21, 23);
            this.lblBruto.Name = "lblBruto";
            this.lblBruto.Size = new System.Drawing.Size(66, 13);
            this.lblBruto.TabIndex = 13;
            this.lblBruto.Text = "Salário bruto";
            // 
            // txtInss
            // 
            this.txtInss.Enabled = false;
            this.txtInss.Location = new System.Drawing.Point(128, 48);
            this.txtInss.Name = "txtInss";
            this.txtInss.Size = new System.Drawing.Size(100, 20);
            this.txtInss.TabIndex = 20;
            this.txtInss.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblInss
            // 
            this.lblInss.AutoSize = true;
            this.lblInss.Location = new System.Drawing.Point(21, 51);
            this.lblInss.Name = "lblInss";
            this.lblInss.Size = new System.Drawing.Size(32, 13);
            this.lblInss.TabIndex = 19;
            this.lblInss.Text = "INSS";
            // 
            // txtLiquido
            // 
            this.txtLiquido.Enabled = false;
            this.txtLiquido.Location = new System.Drawing.Point(128, 76);
            this.txtLiquido.Name = "txtLiquido";
            this.txtLiquido.Size = new System.Drawing.Size(100, 20);
            this.txtLiquido.TabIndex = 22;
            this.txtLiquido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblLiquido
            // 
            this.lblLiquido.AutoSize = true;
            this.lblLiquido.Location = new System.Drawing.Point(21, 79);
            this.lblLiquido.Name = "lblLiquido";
            this.lblLiquido.Size = new System.Drawing.Size(74, 13);
            this.lblLiquido.TabIndex = 21;
            this.lblLiquido.Text = "Salário líquido";
            // 
            // btnCalcular
            // 
            this.btnCalcular.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCalcular.Location = new System.Drawing.Point(152, 104);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 23;
            this.btnCalcular.Text = "&Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // frmExercicio05
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 146);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.txtLiquido);
            this.Controls.Add(this.lblLiquido);
            this.Controls.Add(this.txtInss);
            this.Controls.Add(this.lblInss);
            this.Controls.Add(this.txtBruto);
            this.Controls.Add(this.lblBruto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExercicio05";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercício 05";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBruto;
        private System.Windows.Forms.Label lblBruto;
        private System.Windows.Forms.TextBox txtInss;
        private System.Windows.Forms.Label lblInss;
        private System.Windows.Forms.TextBox txtLiquido;
        private System.Windows.Forms.Label lblLiquido;
        private System.Windows.Forms.Button btnCalcular;
    }
}