﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_03
{
    public partial class frmExercicio02 : Form
    {
        public frmExercicio02()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            float c;
            c = (float.Parse(txtValor.Text) - 32) * 5 / 9;
            txtResultado.Text = c.ToString();
        }

    }
}
