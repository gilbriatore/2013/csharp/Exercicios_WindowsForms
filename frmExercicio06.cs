﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_03
{
    public partial class frmExercicio06 : Form
    {
        public frmExercicio06()
        {
            InitializeComponent();
        }

        private void btnTrocar_Click(object sender, EventArgs e)
        {
            int n1, n2, aux;
            n1 = int.Parse(txtN1.Text);
            n2 = int.Parse(txtN2.Text);
            aux = n1;
            n1 = n2;
            n2 = aux;
            txtFinalN1.Text = n1.ToString();
            txtFinalN2.Text = n2.ToString();
        }
    }
}
