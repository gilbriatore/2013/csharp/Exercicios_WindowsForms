﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_03
{
    public partial class frmExercicio03 : Form
    {
        public frmExercicio03()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            float horas, valor, salario;
            horas = float.Parse(txtHoras.Text);
            valor = float.Parse(txtValor.Text);
            salario = horas * valor;
            txtSalario.Text = salario.ToString();
        }

    }
}
