﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_03
{
    public partial class frmExercicio04 : Form
    {
        public frmExercicio04()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            float b1, b2, b3, b4;
            b1 = float.Parse(txtB1.Text);
            b2 = float.Parse(txtB2.Text);
            b3 = float.Parse(txtB3.Text);
            b4 = 24 - (b1 + b2 + b3);
            txtB4.Text = b4.ToString();
        }
    }
}
